package com.android.learnkotlin


/*fun main(args: Array<String>) {
    val numberRegex = "\\d+".toRegex()
    println(numberRegex.matches("29")) // выведет "true"

    val isNumber = numberRegex::matches
    println(isNumber("29")) // выведет "true"

    val strings = listOf("abc", "124", "a70")
    println(strings.filter(numberRegex::matches)) // выведет "[124]"
    println(strings.filter { n -> numberRegex.matches(n)}) // выведет "[124]"

}*/
//
//    fun <A, B, C> compose(f: (B) -> C, g: (A) -> B): (A) -> C {
//        return { x -> f(g(x)) }
//    }
//
//fun length(s: String) = s.length
//
//val oddLength = compose(::isOdd, ::length)
//val strings = listOf("a", "ab", "abc")


class A(val p: Int)

fun main(args: Array<String>) {
    val prop = A::p
    println(prop.get(A(1))) // выведет "1"
    println(A(1).p)
}

